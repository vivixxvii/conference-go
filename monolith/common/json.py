from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DataEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DataEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        if isinstance(o, self.model):
        #   model property, then
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] =  o.get_api_url()
            for property in self.properties:
        #         * get the value of that property from the model instance
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
        #     * return the dictionary
            return d
        #   otherwise,
        else:
        #       return super().default(o)  # From the documentation
            return super().default(o)

    def get_extra_data(self, o):
        return {}